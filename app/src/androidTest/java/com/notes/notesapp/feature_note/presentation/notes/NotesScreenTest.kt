package com.notes.notesapp.feature_note.presentation.notes

import com.notes.notesapp.di.hilt.AppModule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Assert.*

@HiltAndroidTest
@UninstallModules(AppModule::class)
class NotesScreenTest {

}