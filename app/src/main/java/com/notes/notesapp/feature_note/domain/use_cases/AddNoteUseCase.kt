package com.notes.notesapp.feature_note.domain.use_cases

import com.notes.notesapp.feature_note.domain.model.InvalidNoteException
import com.notes.notesapp.feature_note.domain.model.Note
import com.notes.notesapp.feature_note.domain.repository.NoteRepository
import kotlin.jvm.Throws

class AddNoteUseCase(
    private val repository: NoteRepository
) {

    @Throws(InvalidNoteException::class)
    suspend operator fun invoke(note: Note) {
        if(note.title.isBlank()){
            throw InvalidNoteException("Заголовок не может быть пустым.")
        }
        if(note.content.isBlank()){
            throw InvalidNoteException("Заметка не может быть пустой.")
        }
        repository.insertNote(note)
    }

}